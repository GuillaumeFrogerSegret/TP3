from ClassMud import *

def test_Box_create():
    b=Box()
    
def test_Box_add():
    b=Box()
    b.add("truc1")
    b.add("truc2")
    
    
    assert "truc1" in b
    assert "truc3" not in b

def test_Box_open():
    b=Box()
    assert b.is_open()
    b.close()
    assert not b.is_open()
    assert b.action_look() == "la boite est fermee"
    b.open()
    assert b.is_open()
    
def test_Box_volume():
    b=Box()
    assert b.capacity() ==None
    b.set_capacity(5)
    assert b.capacity() == 5

def test_Things_volume():
    t=Things(5)
    assert t.volume() == 5

def test_Box_has_room_for():
    t=Things(5)
    b=Box()
    assert b.has_room_for(t)
    b.set_capacity(3)
    assert not b.has_room_for(t)
    b.action_add(t)
    assert not t in b
    b.set_capacity(5)
    assert b.has_room_for(t)
    b.action_add(t)
    assert t in b
    

def test_Things_repr():
    t=Things(5)
    t.set_name("chose")
    assert repr(t) == "<chose, 5>"
    assert not t.has_name("truc")
