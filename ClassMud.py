class Box:
    def __init__(self,capacite=None,ouvert=True):
        self._contents=[]
        self._open=ouvert
        self._capacite=capacite
        self._room=capacite
        
    def __repr__(self):
        return "<Box %s>" % (id(self))
        
    def add(self,truc):
        if not self._open:
            print("Boite Fermée")
            return False
        else:
            self._contents.append(truc)
            return True
    
    def __contains__(self,machin):
        return machin in self._contents
    
    def remove(self,truc):
        if truc in self:
            del(self._contents,truc)
            return True
        else:
            return False
    
    def is_open(self):
        return self._open
    
    def open(self):
        self._open=True    
        return True
    
    def close(self):
        self._open=False
        return True
        
    def action_look(self):
        if self.is_open():
            return ("la boite contient: ",self._contents)
        else:
            return "la boite est fermee"
    
    def set_capacity(self,volume):
        self._capacite=volume
        self._room=volume
    
    def capacity(self):
        return self._capacite
    
    def has_room_for(self,t):
        if self._room==None:
            return True
        else:
            if self._room-t._volume>=0:
                return True
            else:
                return False
            
    def action_add(self,t):
        if self.has_room_for(t):
            if self._room!=None:
                self._room-=t._volume
            self.add(t)
            return True
        else:
            return False
    
    def find(self,name):
        if not self.is_open():
            return None
        else:
            for t in self._contents:
                if t.has_name(name):
                    return t       

    @staticmethod
    def from_data(data):
        o=data.get("open")
        c=data.get("capacite")
        return Box(ouvert=o, capacite=c)
     

class Things:
    
    def __init__(self,volume, name=None):
        self._volume=volume
        self._name=name
    
    def __repr__(self):
        return "<%s, %s>" % (self._name,self._volume)

    def __contains__(self,machin):
        return machin in self._contents
    
    def volume(self):
        return self._volume
    
    def set_name(self,name):
        self._name=name
    
    def has_name(self,nom):
        return self._name==nom

    @staticmethod
    def from_data(data):
        v=data.get("volume")
        n=data.get("nom")
        return Things(volume=v, name=n)